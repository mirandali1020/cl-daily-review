const models = require('@cxz/cl-models')
const config = require('config')
const mongoose = require('mongoose')
const _ = require('lodash')
var Vehicle, User, Promotion

module.exports.create = function () {
  const bootstrap = function (models, mongoose) {
    models.mongo.init(mongoose)
    return models.mongo.connect(config.mongo.db, function (err) {
      console.error('Mongo connection error: ', err)
    })
      .then(() => {
        return
      })
  }
  const getAllPromotion = function () {
    return Promotion.find({})
      .then((promotions) => {
        if (promotions) {
          return promotions
        }
        else {
          Promise.reject('no promotion in db')
        }
      })
  }
  const checkAvailability = function (singlePromotion) {
    try {
      if (singlePromotion.startDate && singlePromotion.startDate > new Date()) {
        console.error('unexpected startDate for promotion: ', singlePromotion)
        return false
      }
      if (singlePromotion.endDate && singlePromotion.endDate > new Date()) {
        console.error('unexpected endDate for promotion: ', singlePromotion)
        return false
      }
      if (!singlePromotion.endDate) {
        return true
      } else {
        return false
      }
    } catch (error) {
      console.error('Error promotion record:', singlePromotion, error)
      return false
    }
  }
  const getPromotedUser = function (vin) {
    return Vehicle.findOneAsync({
      vin: vin
    })
      .then((vehicle) => {
        if (vehicle && vehicle.user) {
          return vehicle.user
        } else {
          return
        }
      })
      .catch((err) => {
        console.error('mongo error searching for vin:', vin, err)
        Promise.reject('mongo error searching for vin:', vin)
      })
  }
  const init = function (mongoose) {
    Promotion = mongoose.model('Promotion')
    Vehicle = mongoose.model('Vehicle')
    User = mongoose.model('User')
    return
  }
  const main = function () {
    var listOfUser = []
    console.log(Promotion)
    return getAllPromotion()
      .then((promotions) => {
        const validPromotion = _.filter(promotions, checkAvailability)
        var checkPromise = []
        validPromotion.forEach(function (singlePromotion) {
          checkPromise.push(getPromotedUser(singlePromotion))
        })
        return Promise.all(checkPromise)
      })
      .then((userList) => {
        listOfUser = _.uniq(userList)
        return listOfUser
      })
      .catch((err) => {
        throw err
      })
  }
  const dailyCheckTask = {
    bootstrap: bootstrap,
    getAllPromotion: getAllPromotion,
    getPromotedUser: getPromotedUser,
    checkAvailability: checkAvailability,
    init: init,
    main: main
  }
  return dailyCheckTask
}