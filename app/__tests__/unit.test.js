'use strict'
/* globals describe,it,expect,beforeEach,before,afterEach */

const chai = require('chai')
const sinon = require('sinon')
const sinonChai = require('sinon-chai')
const moment = require('moment')
const config = require('config')
const _ = require('lodash')

global.expect = chai.expect
global.sinon = sinon

chai.use(sinonChai)




describe('dailyCheckTask', function () {
  describe('checkAvailability(singlePromotionObj)', function () {
    const dailyCheckTask = require('../dailyCheckTask.js').create()
    it('should return true when the promotion is not started (start:null, end:null)', function () {
      const singlePromotion = {
        vin: '1G6AR5SXXG0143486',
        promotionName: 'BusinessChoice',
        numberOfFreeMonths: 3,
        startDate: '',
        endDate: ''
      }
      expect(dailyCheckTask.checkAvailability(singlePromotion)).to.be.true
    })
    it('should return true when the promotion is not ended (start:past, end:null)', function () {
      const singlePromotion = {
        vin: '1G6AR5SXXG0143486',
        promotionName: 'BusinessChoice',
        numberOfFreeMonths: 3,
        startDate: moment().subtract(1, 'day'),
        endDate: ''
      }
      expect(dailyCheckTask.checkAvailability(singlePromotion)).to.be.true
    })
    it('should return false when the promotion is ended (start:past, end:past)', function () {
      const singlePromotion = {
        vin: '1G6AR5SXXG0143486',
        promotionName: 'BusinessChoice',
        numberOfFreeMonths: 3,
        startDate: moment().subtract(2, 'day'),
        endDate: moment().subtract(1, 'day')
      }
      expect(dailyCheckTask.checkAvailability(singlePromotion)).to.be.false
    })
    it('should return false when (start: future, end: whatever)', function () {
      const singlePromotion = {
        vin: '1G6AR5SXXG0143486',
        promotionName: 'BusinessChoice',
        numberOfFreeMonths: 3,
        startDate: moment().add(1, 'day'),
        endDate: ''
      }
      expect(dailyCheckTask.checkAvailability(singlePromotion)).to.be.false
    })
    it('should return false when (start: whatever, end: future)', function () {
      const singlePromotion = {
        vin: '1G6AR5SXXG0143486',
        promotionName: 'BusinessChoice',
        numberOfFreeMonths: 3,
        startDate: '',
        endDate: moment().add(1, 'day')
      }
      expect(dailyCheckTask.checkAvailability(singlePromotion)).to.be.false
    })
  })

  describe('getAllPromotion()', function () {
    const dailyCheckTask = require('../dailyCheckTask.js').create()
    const models = require('@cxz/cl-models')
    const mongoose = require('mongoose')
    const promotionNum = 100
    var Promotion
    it('should return all the promotions in db', function () {
      return dailyCheckTask.bootstrap(models, mongoose)
        .then(function () {
          Promotion = mongoose.model('Promotion')
          var PromotionSave = []
          for (var i = 0; i < promotionNum; i++) {
            PromotionSave.push(Promotion.insert('1G6AR5SXXG014348' + i, 'BusinessChoice', '3'))
          }
          return Promise.all(PromotionSave)
        })
        .then(() => {
          return dailyCheckTask.getAllPromotion(Promotion)
        })
        .then((promotions) => {
          expect(promotions).to.have.length(promotionNum)
        })
    })
  })
  describe('getPromotedUser()', function () {
  })

  describe.only('main()', function () {
    const dailyCheckTask = require('../dailyCheckTask.js').create()
    const models = require('@cxz/cl-models')
    const mongoose = require('mongoose')
    
    var Vehicle, Promotion, User
    before(function () {
      this.timeout(25000)
      return dailyCheckTask.bootstrap(models, mongoose)
        .then(() => {
          Promotion = mongoose.model('Promotion')
          Vehicle = mongoose.model('Vehicle')
          User = mongoose.model('User')
          return
        })
        .catch((err) => {
          console.log(err)
        })
    })
    beforeEach(function () {
      return models.mongo.clear()
    })
    it('should return list of user with valid promotion record', function () {
      //data prepare:
      var fixtures = models.mongo.fixtures
      var user1 = mongoose.Types.ObjectId('566250843f8bb0cba303e491')
      var user2 = mongoose.Types.ObjectId('566250843f8bb0cba303e492')
      var user3 = mongoose.Types.ObjectId('566250843f8bb0cba303e493')

      var vehicles = [
        _.assign(fixtures.vehicles.taxi(), {
          vin: '1',
          user: user1
        }),
        _.assign(fixtures.vehicles.taxi2(), {
          vin: '2',
          user: user1
        }),
        _.assign(fixtures.vehicles.truck(), {
          vin: '3',
          user: user2
        }),
        _.assign(fixtures.vehicles.fanTaxi(), {
          vin: '4',
          user: user3
        })
      ]
      var savePromise = []
      savePromise.push(Vehicle.create(vehicles))
      savePromise.push(Promotion.insert(vehicles[0].vin, 'BusnessChoice', 3))
      savePromise.push(Promotion.insert(vehicles[1].vin, 'BusnessChoice', 3))
      savePromise.push(Promotion.insert(vehicles[2].vin, 'BusnessChoice', 3))
      savePromise.push(Promotion.insert(vehicles[3].vin, 'BusnessChoice', 3))
      dailyCheckTask.init(mongoose)
      return Promise.all(savePromise)
        .then((res) => {
        return dailyCheckTask.main(models, mongoose)
        })
        .then((userList) => {
          console.log(userList)
        })
    })
  })
})