'use strict'

const fs = require('fs')
const path = require('path')
const gulp = require('gulp')

const TASKS_DIR = path.resolve(path.join(__dirname, '/config/gulp/tasks/'))

fs.readdirSync(TASKS_DIR).forEach(function (filename) {
  require(`${TASKS_DIR}/${filename}`)
})

gulp.task('test', ['mocha'])
gulp.task('lint', ['lint:js'])
gulp.task('start', ['env:development', 'nodemon'])
gulp.task('s', ['start'])

gulp.task('default', ['start'])