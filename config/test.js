'use strict'

module.exports = {
  port: 3012,
  mongo: {
    db: 'mongodb://localhost/flex-test'
  }
}
