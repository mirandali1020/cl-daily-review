'use strict'

module.exports = {
  mongo: {
    db: process.env.MONGO_DB_URL || process.env.MONGOHQ_URL || 'mongodb://localhost/current_prod'
  }
}