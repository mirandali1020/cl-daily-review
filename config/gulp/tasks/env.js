'use strict'

const gulp = require('gulp')
const fs = require('fs')
const ini = require('ini')
const _ = require('lodash')

let loadEnvFromIni, loadParsedEnv

gulp.task('env:test', function () {
  process.env.NODE_ENV = 'test'
  process.env.NEW_RELIC_NO_CONFIG_FILE = true
  process.env.HEARTBEAT_ENABLED = false
  process.env.LOGGLY_CUSTOMER_TOKEN = ''
})

gulp.task('env:development', function () {
  process.env.NODE_ENV = 'development'
  loadEnvFromIni('.env')
})

loadEnvFromIni = function (file) {
  let env

  try {
    env = ini.parse(fs.readFileSync(file, 'utf-8')) || {}
  } catch (e) {
    console.warn('Missing .env')
    env = {}
  }

  loadParsedEnv(env)
}

loadParsedEnv = function (env) {
  _.forEach(env, function (value, property) {
    if (_.isObject(value)) {
      loadParsedEnv(value)
    } else {
      process.env[property] = value
    }
  })
}
