'use strict'

const gulp = require('gulp')
const nodemon = require('nodemon')

gulp.task('nodemon', function () {
  nodemon({
    script: 'server.js',
    watch: ['app', 'config', '.env']
  })
})
