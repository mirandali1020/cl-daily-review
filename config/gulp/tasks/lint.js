'use strict'

const gulp = require('gulp')
const path = require('path')
const chalk = require('chalk')
const standard = require('standard')

gulp.task('lint:js', function (cb) {
  const FILES = path.resolve(path.join(__dirname, '../../../**/*.js'))

  const opts = {
    ignore: [path.join(__dirname, '../../../app/web/**'),
             path.join(__dirname, '../../../webpack.config.js')]
  }

  standard.lintFiles(FILES, function (err, res) {
    if (err) { return cb(err) }

    if (res.errorCount) {
      res.results.forEach(function (test) {
        if (test.errorCount) {
          console.log('\n\t%s', chalk.yellow('LINTER ERRORS:'))

          test.messages.forEach(function (message) {
            console.log('\t%s:%s:%s: %s',
              chalk.magenta(test.filePath),
              message.line,
              message.column,
              chalk.red(message.message)
            )
          })

          console.log('\n')
        }
      })

      return cb(new Error('Standard lint failed'))
    }

    cb()
  }, opts)
})
