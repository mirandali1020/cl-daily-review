'use strict'

const gulp = require('gulp')
const spawn = require('child_process').spawn
const glob = require('glob')
const path = require('path')
const argv = require('minimist')(process.argv.slice(2))

const SETUP_FILE = path.resolve(path.join(
  __dirname, '../../../app/__tests__/setup.js'))
const TEST_FILES_GREP = path.resolve(path.join(
  __dirname, '../../../app/**/*.test.js'))

/* Gulp mocha doesn't support require ATM */
gulp.task('mocha', function (cb) {
  process.env.NODE_ENV = 'test'

  const files = [SETUP_FILE].concat(
    glob.sync(argv.grep || TEST_FILES_GREP)
  )

  const args = files.concat(['--colors'])

  const mocha = spawn('./node_modules/.bin/mocha', args)

  mocha.stdout.on('data', function (data) {
    process.stdout.write(data.toString())
  })

  mocha.stderr.on('data', function (data) {
    process.stdout.write(data.toString())
  })

  mocha.on('error', function (err) {
    console.log('MOCHA ERROR', err)
  })

  mocha.on('exit', function (code) {
    let err = null

    if (code !== 0) {
      err = new Error('Mocha backend tests failed')
    }

    cb(err)
  })
})
